# アレクサ、百人一首シャッフルで一句

[![pipeline status](https://gitlab.com/tomari/alexa-hyakunin-shuffle/badges/master/pipeline.svg)](https://gitlab.com/tomari/alexa-hyakunin-shuffle/commits/master)

通常、百人一首は100の和歌のどれかを読み上げるわけですが、このスキルでは5/7/5/7/7 の各セグメントを別々にシャッフルします。結果として、大変多くのパターンが生まれ、先を予想することは非常に困難になります。

## デプロイ方法

まず、Alexa Developer Console からポチポチとやる

`scripts/deploy.sh` のような感じでアップデートしたら自動でデプロイしていくと良い。
