'use strict';
const Alexa=require('alexa-sdk');
const ByeMessage='さようなら。';
const AppletName='百人一首シャッフル';
const HelpMessage='一句詠んで、というと、百人一首の和歌のそれぞれの句をシャッフルして読み上げます。どうしますか？';

// cut-n-paste from MDN
function getRandomInt(max){
	return Math.floor(Math.random() * Math.floor(max));
}
function ssmlHyakuninIsshu(ku){
	const break_ms=[160,120,500,100];
	return ku.map((x,i)=>x+(break_ms[i]?'<break time="'+break_ms[i]+'ms"/> ':'')).join('');
}
var handlers={
	'LaunchRequest':function(){
		this.emit(':ask',AppletName+'にようこそ。'+HelpMessage,HelpMessage);
	},
	'ShuffleIntent':function(){
		const hyakunin=require('ogura-hyakunin-isshu');
		let nh=hyakunin.length;
		var idx_arr=[];
		for(var i=0;i<5;i++){idx_arr.push(getRandomInt(nh));}
		var speechRes=ssmlHyakuninIsshu(idx_arr.map((x,i) => hyakunin[x].ruby[i]));
		var cardRes=idx_arr.map((x,i) => hyakunin[x].text[i]).join(' ')+'\n'+
			idx_arr.map((x,i) => hyakunin[x].n+" "+hyakunin[x].author.name).join('/');
		this.response.cardRenderer(AppletName,cardRes);
		this.response.speak(speechRes);
		this.emit(':responseReady');
	},
	'AMAZON.HelpIntent':function(){
		this.emit(':ask',HelpMessage,HelpMessage);
	},
	'AMAZON.CancelIntent':function(){
		this.emit(':tell',ByeMessage);
	},
	'AMAZON.StopIntent':function(){
		this.emit(':tell',ByeMessage);
	},
	'Unhandled':function(){
		this.emit(':tell','わからない。');
	}
};
exports.handler=function(event,context,callback){
	var alexa=Alexa.handler(event,context);

	//alexa.appId='';
	//alexa.dynamoDBTableName=''; // session mgmt

	alexa.registerHandlers(handlers);
	alexa.execute();
};
