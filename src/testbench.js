'use strict';
const alexaTest=require('alexa-skill-test-framework');
alexaTest.initialize(
	require('./index.js'),"amzn1.ask.skill.00000000-0000-0000-0000-000000000000","amzn1.ask.account.VOID"
);
alexaTest.initializeI18N({});
alexaTest.setLocale("ja-JP");

function printResponse(context,utterance){
	console.log(utterance);
}

describe("LaunchRequest and CancelIntent",function(){
	alexaTest.test([
		{ request: alexaTest.getLaunchRequest(), shouldEndSession: false, repromptNothing:false,
		  saysCallback: printResponse },
		{ request: alexaTest.getIntentRequest('AMAZON.CancelIntent'), shouldEndSession:true,
		  saysCallback: printResponse }
	]);
});

describe("LaunchRequest and StopIntent",function(){
	alexaTest.test([
		{ request: alexaTest.getLaunchRequest(), shouldEndSession: false, repromptNothing:false,
		  saysCallback: printResponse },
		{ request: alexaTest.getIntentRequest('AMAZON.StopIntent'), shouldEndSession:true,
		  saysCallback: printResponse }
	]);
});

describe("LaunchRequest and SessionEndedRequest",function(){
	alexaTest.test([
		{ request: alexaTest.getLaunchRequest(), shouldEndSession: false, repromptNothing:false,
		  saysCallback: printResponse },
		{ request: alexaTest.getSessionEndedRequest('USER_INITIATED'), shouldEndSession:true,
		  saysCallback: printResponse }
	]);
});

describe("AMAZON.HelpIntent",function(){
	alexaTest.test([
		{ request: alexaTest.getIntentRequest("AMAZON.HelpIntent"), shouldEndSession: false,
		  saysCallback: printResponse }
	]);
});

describe("ShuffleIntent",function(){
	alexaTest.test([
		{ request: alexaTest.getIntentRequest('ShuffleIntent'), shouldEndSession: true,
		  saysCallback: printResponse }
	]);
});


